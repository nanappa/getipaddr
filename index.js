
var net = require('net');

function getIPAddr(target = 'www.google.com', port=80) {
	return new Promise((resolve, reject) => {
		var socket = net.createConnection(port, target);
		socket.on('connect', function() {
			let addr = socket.address().address;
			socket.end();
			return resolve(addr)
		});
		socket.on('error', function(e) {
			return reject(e)
		});
	})
}

module.exports = getIPAddr;


